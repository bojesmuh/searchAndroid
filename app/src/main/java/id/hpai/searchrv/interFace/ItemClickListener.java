package id.hpai.searchrv.interFace;

import android.view.View;

/**
 * Created by Admin on 07/02/18.
 */

public abstract class ItemClickListener {

    //void onItemClick(View v, int pos);

    public abstract void onItemClick(View v, int pos);
}
